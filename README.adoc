= Rook/Ceph deploy via GitOps

This GitOps deployment of Rook/Ceph includes the following steps:

1. Deploy the Rook Operator using Helm:
```
helm repo add rook-release https://charts.rook.io/release
helm install rook-ceph rook-release/rook-ceph
```

2. Create a Ceph Cluster:
```
kubectl create -f cluster.yaml
```

3. Create a Ceph Filesystem:
```
kubectl create -f filesystem.yaml
```

4. Create a Ceph Block Storage:
```
kubectl create -f block.yaml
```

5. Create a Ceph Object Store:
```
kubectl create -f object.yaml
```

6. Expose the Ceph Object Store:
```
kubectl create -f object-service.yaml
```

7. Monitor the Rook/Ceph deployment:
```
kubectl get pods -n rook-ceph
```

8. Monitor

== Docker Lock

[source,bash]
----
docker-lock lock generate --kubernetesfiles='deploy/toolbox.yaml,deploy/common/operator.yaml,deploy/cluster/cephcluster.yaml'
----
